FROM amazoncorretto:17-alpine3.19

WORKDIR /usr/local/app
COPY build/libs/springboot-helloworld-0.0.1-SNAPSHOT.jar ./application.jar

USER spring

CMD ["java", "-jar ./application.jar"]